const wordsUrl = "/words"
var ws;
var recInterval = null;
var newConn = function () {

    ws = new SockJS(wordsUrl);

    ws.onmessage = function (event) {
        var wordsListElement = $("#words");
        wordsListElement.empty();
        var words = JSON.parse(event.data);
        for (var i in words) {
            //bug in bs 4.1 not displaying items using li
            wordsListElement.append($("<a href=\"#\" class=\"list-group-item disabled\"/>").text(words[i]));
        }
    };

    clearInterval(recInterval);

    ws.onerror = function (event) {
        console.log(event.data);
        alert('Error: ' + event.data);
    };
    ws.onopen = function () {
        console.log("ws opened");
    };
    ws.onclose = function () {
        console.log("ws closed");
        ws = null;
        recInterval = setInterval(function () {
            newConn();
        }, 2000);
    };
}

newConn();

$(document).ready(function () {
    $("#words-form").submit(function (event) {
        event.preventDefault();
        if (ws != null) {
            var message = $('#message').val();
            ws.send(message);
            console.log("Word sent : " + message);
        } else {
            alert('WebSocket connection not established, please connect.');
        }
        return false;
    });
});
