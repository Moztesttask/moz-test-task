package by.reghor.moztest.service;

import by.reghor.moztest.WordsException;
import by.reghor.moztest.dao.WordsDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.regex.Pattern;

@Service
@Transactional
public class WordsService {

    private static final Pattern WORD_PATTERN = Pattern.compile("\\w{1,50}");

    @Autowired
    private WordsDao wordsDao;

    public List<String> saveWord(String word) throws WordsException {
        if (StringUtils.isNotBlank(word) && WORD_PATTERN.matcher(word).matches()) {
            wordsDao.save(StringUtils.reverse(word));
        } else {
            throw new WordsException("please provide a valid word");
        }
        return wordsDao.getAll();
    }
}
