package by.reghor.moztest.dao;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class WordsDao {

    private static final String INSERT_QUERY ="INSERT INTO words (word) VALUES (?)";
    private static final String SELECT_QUERY ="SELECT word FROM words";

    @PersistenceContext
    private EntityManager entityManager;

    public void save(String word) {
        entityManager.createNativeQuery(INSERT_QUERY).setParameter(1, word).executeUpdate();
    }

    public List<String> getAll() {
        return entityManager.createNativeQuery(SELECT_QUERY).getResultList();
    }
}
