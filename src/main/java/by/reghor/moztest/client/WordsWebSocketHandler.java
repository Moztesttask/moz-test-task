package by.reghor.moztest.client;

import by.reghor.moztest.service.WordsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.List;

public class WordsWebSocketHandler extends TextWebSocketHandler {

    @Autowired
    private WordsService wordsService;
    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        String wordToSave = message.getPayload();
        List<String> savedWords = wordsService.saveWord(wordToSave);
        session.sendMessage(new TextMessage(objectMapper.writeValueAsString(savedWords)));
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        session.close(CloseStatus.SERVER_ERROR);
    }

}
