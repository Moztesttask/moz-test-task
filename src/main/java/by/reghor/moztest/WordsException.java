package by.reghor.moztest;

public class WordsException extends Exception{
    public WordsException() {
        super();
    }

    public WordsException(String message) {
        super(message);
    }

    public WordsException(String message, Throwable cause) {
        super(message, cause);
    }

    public WordsException(Throwable cause) {
        super(cause);
    }

    protected WordsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
